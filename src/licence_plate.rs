use rand::Rng;

const CHARS: [&str; 26] = [
    "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S",
    "T", "U", "V", "W", "X", "Y", "Z",
];

fn generate_licence_chars() -> [&'static str; 4] {
    let mut rng = rand::thread_rng();
    [
        CHARS[rng.gen_range(0..25)],
        CHARS[rng.gen_range(0..25)],
        CHARS[rng.gen_range(0..25)],
        CHARS[rng.gen_range(0..25)],
    ]
}

fn generate_licence_nums() -> [u32; 3] {
    let mut rng = rand::thread_rng();
    [
        rng.gen_range(1..9),
        rng.gen_range(1..9),
        rng.gen_range(1..9),
    ]
}

/// Returns a random licence plate
///
/// # Examples
///
/// ```
/// use licence_plate;
/// let plate = licence_plate::licence_plate_gen();
/// println!(plate); // => ex: AA-123-BB
/// ```
pub fn licence_plate_gen() -> String {
    let chars = generate_licence_chars();
    let nums = generate_licence_nums();
    format!(
        "{}{}-{}{}{}-{}{}",
        chars[0], chars[1], nums[0], nums[1], nums[2], chars[2], chars[3]
    )
}

#[cfg(test)]
mod tests {
    use crate::licence_plate;
    use regex::Regex;
    #[test]
    fn generate_licence_plate() {
        let re = Regex::new(r"[[:alpha:]]{2}-[[:digit:]]{3}-[[:alpha:]]{2}").unwrap();
        assert!(re.is_match(licence_plate::licence_plate_gen().as_str()));
    }
}
