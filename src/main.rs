mod licence_plate;
mod parking;

use rand::Rng;
use std::{thread, time};

fn main() {
    println!("Welcome to the parking lot challenge !");
    let size: usize = 8;
    let mut parking = parking::Parking::new(size).unwrap();
    let mut cars: Vec<String> = Vec::new();

    let mut rng = rand::thread_rng();

    for n in 0..100 {
        println!("Iteration {}\n", n);
        let car = licence_plate::licence_plate_gen();

        if !parking.park(&car) {
            let local_car_ind = rng.gen_range(0..cars.len() - 1);
            let random_car = &cars[local_car_ind];
            parking.remove(random_car.clone()).unwrap();
            cars.remove(local_car_ind);
            parking.park(&car);
        }
        cars.push(car);
        thread::sleep(time::Duration::from_millis(1500));
    }
}
