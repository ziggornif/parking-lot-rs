use std::error::Error;
use std::usize;

#[derive(Debug)]
pub struct Parking {
    slots: Vec<String>,
    size: usize,
}

impl Parking {
    /// Instantiate a new Parking
    ///
    /// # Arguments
    ///
    /// * `size` - The parking size
    ///
    /// # Examples
    ///
    /// ```
    /// use parking;
    /// let parking = parking::Parking::new(size);
    /// ```
    pub fn new(size: usize) -> Result<Parking, Box<dyn Error>> {
        println!("Build parking with {} slots\n", size);
        if size == 0 {
            return Err("Parking size must be > 0")?;
        }
        Ok(Parking {
            slots: Vec::with_capacity(size),
            size,
        })
    }

    /// Park a new car
    ///
    /// # Arguments
    ///
    /// * `car_id` - The car to park
    ///
    /// # Examples
    ///
    /// ```
    /// use parking;
    /// parking.park(&car);
    /// ```
    pub fn park(&mut self, car_id: &String) -> bool {
        match self.find_available_park() {
            None => {
                if self.slots.len() == self.size {
                    println!("🛑 Parking is full\n");
                    return false;
                }
                self.slots.push(car_id.clone());
                println!("🚗 {} join an opening slot\n", car_id);
            }
            Some(index) => {
                self.slots[index] = car_id.clone();
                println!("🚗 {} join the slot no {}\n", car_id, index);
            }
        }

        self.print_park();
        return true;
    }

    /// Remove a parked car
    ///
    /// # Arguments
    ///
    /// * `car_id` - The car to remove
    ///
    /// # Examples
    ///
    /// ```
    /// use parking;
    /// parking.remove(car);
    /// ```
    pub fn remove(&mut self, car_id: String) -> Result<(), Box<dyn Error>> {
        match self.slots.iter().position(|r| r == &car_id) {
            None => Err(format!("Car {} not found", car_id))?,
            Some(index) => {
                self.slots[index] = String::new();
                println!("🚙 {} left the slot no {}\n", car_id, index);
                self.print_park();
                Ok(())
            }
        }
    }

    /// Print parking
    ///
    /// # Examples
    ///
    /// ```
    /// use parking;
    /// parking.print_park();
    /// ```
    pub fn print_park(&self) {
        println!("Current parking slots :");
        for (pos, e) in self.slots.iter().enumerate() {
            println!("Slot {} : {}", pos, e);
        }
        println!("");
    }

    fn find_available_park(&self) -> Option<usize> {
        return self.slots.iter().position(|r| *r == String::new());
    }
}

#[cfg(test)]
mod tests {
    use crate::licence_plate;
    use crate::parking;

    #[test]
    fn create_new_parking() {
        let parking = parking::Parking::new(2).unwrap();
        assert_eq!(parking.size, 2);
    }

    #[test]
    fn create_new_parking_size_error() {
        let err = parking::Parking::new(0).unwrap_err().to_string();
        assert_eq!(err, "Parking size must be > 0");
    }

    #[test]
    fn park_car() {
        let mut parking = parking::Parking::new(1).unwrap();
        let car = "AA-123-BB".to_owned();
        let park_res = parking.park(&car);
        assert_eq!(park_res, true);
    }

    #[test]
    fn park_on_parking_full() {
        let mut parking = parking::Parking::new(1).unwrap();
        let car = "AA-123-BB".to_owned();
        parking.park(&car);

        let car2 = "CC-456-DD".to_owned();
        let park_res = parking.park(&car2);
        assert_eq!(park_res, false);
    }

    #[test]
    fn remove_car() {
        let mut parking = parking::Parking::new(1).unwrap();
        let car = "AA-123-BB".to_owned();
        parking.park(&car);
        assert!(parking.remove(car).is_ok());
    }

    #[test]
    fn remove_car_error() {
        let mut parking = parking::Parking::new(1).unwrap();
        let car = "AA-123-BB".to_owned();
        let err = parking.remove(car).unwrap_err().to_string();
        assert_eq!(err, "Car AA-123-BB not found");
    }

    #[test]
    fn park_empty_middle_slot() {
        let mut parking = parking::Parking::new(3).unwrap();
        let car1 = licence_plate::licence_plate_gen();
        let car2 = licence_plate::licence_plate_gen();
        let car3 = licence_plate::licence_plate_gen();
        let car4 = licence_plate::licence_plate_gen();
        parking.park(&car1);
        parking.park(&car2);
        parking.park(&car3);
        parking.remove(car2).unwrap();
        parking.park(&car4);
        assert_eq!(parking.slots, [car1, car4, car3]);
    }
}
